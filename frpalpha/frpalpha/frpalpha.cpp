#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "frp.h"

using namespace std;

int main()
{
	enum Stats {STR=1,DEX=2,CON=3,WIS=4,INT=5,CHAR=6};
	FRP player;
	vector<int> stat_holder;
	ofstream output;
	int casei = -1, throw_count, dice_side;
	string s;
	cout << "Enter your Character's name: ";
	cin >> player.Name;
	cout << "Enter your Character's race: ";
	cin >> player.Race;
	cout << "Enter your Character's class: ";
	cin >> player.Class;
	cout << "Enter your Character's deity: ";
	cin >> player.Deity;
	player.char_stat_gen(stat_holder);
	cout << "If you want a full reroll please enter \"reroll\": ";
	cin >> s;
	if (s == "reroll")
	{
		stat_holder.clear();
		player.char_stat_gen(stat_holder);
	}
	s.clear();

	for(int k=0; k<stat_holder.size();)
	{
		player.stat_check(player);
		cout << "Which stat do you want to give " << stat_holder[k] << " point?" << endl;
		cin >> casei;
		switch (casei)
		{
		case STR: 
			player.Strength = stat_holder[k];
			cout << "Your strength grows!" << endl;
			k++;
			player.strcheck = "Checked!";
			break;
		case DEX: 
			player.Dexterity = stat_holder[k];
			cout << "Your dexterity grows!" << endl;
			k++;
			player.dexcheck = "Checked!";
			break;
		case CON: 
			player.Constitution = stat_holder[k];
			cout << "Your constitution grows!" << endl;
			k++;
			player.concheck = "Checked!";
			break;
		case WIS: 
			player.Wisdom = stat_holder[k];
			cout << "Your wisdom grows!" << endl;
			k++;
			player.wischeck = "Checked!";
			break;
		case INT: 
			player.Intelligence = stat_holder[k];
			cout << "Your intelligence grows!" << endl;
			k++;
			player.intcheck = "Checked!";
			break;
		case CHAR:
			player.Charisma = stat_holder[k];
			cout << "Your intelligence grows!" << endl;
			k++;
			player.charcheck = "Checked!";
			break;
		default:
			cout << "Invalid Selection." << endl;
			break;
		}
	}

	cout << "Name: " << player.Name << " | Race: " << player.Race << " | Class: " << player.Class << " | Deity: " << player.Deity << endl;
	cout << "-------------------------------------------------------------------------------------------------" << endl;
	player.show_stats(player);
	output.open("character_sheet.txt");
	output << "Name: " << player.Name << " | Race: " << player.Race << " | Class: " << player.Class << " | Deity: " << player.Deity << endl;
	output << "---------------------------------------------------------------" << endl;
	player.show_stats(player,output);
	output.close();

	bool should_continue = true;
	while(should_continue)
	{
		cout << "Please enter the dice you want to throw(ex. 3d10): " << endl;
		getline(cin,s);
		if (s == "q")
		{
			cin.get();
			cin.ignore();
			break;
		}
		istringstream iss(s);
		iss >> throw_count;
		iss >> dice_side;
		int total_dice_sum=0;
		
		for(int current_throw=0;current_throw<throw_count;current_throw++)
		{
			total_dice_sum += player.random_dice(dice_side);
		}
		cout << total_dice_sum;
	}

	cin.get();
	cin.ignore();
	return 0;
}

