#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "randgen.h"
#include "frp.h"

using namespace std;


void FRP::bubble_sort(int array[], int array_size)
{
	for (int i = 1; i<array_size; i++)
	{
		for (int j = 0; j<array_size-i; j++)
		{
			if(array[j] > array[j+1])
			{
				int temp = array [j];
				array [j] = array [j+1];
				array [j+1] = temp;
			}
		}
	}
}

void FRP::bubble_sort(vector<int> &vector, int vector_size)
{
	for (int i = 1; i<vector_size; i++)
	{
		for (int j = 0; j<vector_size-i; j++)
		{
			if(vector[j] < vector[j+1])
			{
				int temp = vector[j];
				vector[j] = vector[j+1];
				vector[j+1] = temp;
			}
		}
	}
}

int FRP::random_dice(int dice_side)
{
	RandGen rand_num_gen;
	return rand_num_gen.RandInt(1,dice_side);
}

int FRP::stat_calc()
{
	int i[4];
	i[0] = random_dice(6);
	i[1] = random_dice(6);
	i[2] = random_dice(6);
	i[3] = random_dice(6);
	bubble_sort(i,4);
	int stat = i[0]+i[1]+i[2];
	return stat;
}

void FRP::char_stat_gen(vector<int> &vector)
{
	for (int i=0; i<7 ; i++)
	{
		vector.push_back(stat_calc());
	}
	bubble_sort(vector, vector.size());
	vector.pop_back();
}

void FRP::show_stats(FRP player)
{
	cout << "vVv Character Stats vVv" << endl;
	cout << "STR: " << player.Strength << endl;
	cout << "DEX: " << player.Dexterity << endl;
	cout << "CON: " << player.Constitution << endl;
	cout << "WIS: " << player.Wisdom << endl;
	cout << "INT: " << player.Intelligence << endl;
	cout << "CHAR: " << player.Charisma << endl;
}

void FRP::show_stats(FRP player,ofstream &out)
{
	out << "vVv Character Stats vVv" << endl;
	out << "STR: " << player.Strength << endl;
	out << "DEX: " << player.Dexterity << endl;
	out << "CON: " << player.Constitution << endl;
	out << "WIS: " << player.Wisdom << endl;
	out << "INT: " << player.Intelligence << endl;
	out << "CHAR: " << player.Charisma << endl;
}

void FRP::stat_check(FRP player)
{
	cout << "1-STR: " << player.strcheck << endl;
	cout << "2-DEX: " << player.dexcheck << endl;
	cout << "3-CON: " << player.concheck << endl;
	cout << "4-WIS: " << player.wischeck << endl;
	cout << "5-INT: " << player.intcheck << endl;
	cout << "6-CHAR: " << player.charcheck << endl;
}

