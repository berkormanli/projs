#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_dal_enter_clicked();

    void on_kac_dal_valueChanged(int arg1);

    void on_yirmidal_clicked();

    void on_alkolfiyat_valueChanged(int arg1);

    void on_alkoltlekle_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
