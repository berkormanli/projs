#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

static int sigaracount = 0;
static int gecicisigaracount = 0;
static int alkolfiyat = 0;
static int gecicialkolfiyat = 0;

void MainWindow::on_dal_enter_clicked()
{
    sigaracount += gecicisigaracount;
    gecicisigaracount = 0;
    ui->dal_counter->display(sigaracount);
    ui->sigarayazi->setText(200-sigaracount + "<font color='red'>dal hakkın kaldı!</font>");
}

void MainWindow::on_kac_dal_valueChanged(int arg1)
{
    gecicisigaracount = arg1;
}


void MainWindow::on_yirmidal_clicked()
{
    sigaracount += 20;
    ui->dal_counter->display(sigaracount);
    ui->sigarayazi->setText(200-sigaracount + "<font color='red'>dal hakkın kaldı!</font>");
}

void MainWindow::on_alkolfiyat_valueChanged(int arg1)
{
    gecicialkolfiyat += arg1;
}

void MainWindow::on_alkoltlekle_clicked()
{
    alkolfiyat += gecicialkolfiyat;
    gecicialkolfiyat = 0;
    //ui->alkolyazi->setText("Alkole " + 100-alkolfiyat + "TL daha harcayabilirsin!");
}
