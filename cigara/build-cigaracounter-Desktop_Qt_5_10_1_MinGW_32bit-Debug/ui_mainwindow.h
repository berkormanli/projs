/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QTabWidget *kategori;
    QWidget *sigara;
    QGridLayout *gridLayout;
    QLCDNumber *dal_counter;
    QGroupBox *paket;
    QVBoxLayout *verticalLayout_2;
    QPushButton *yirmidal;
    QGroupBox *sarma;
    QVBoxLayout *verticalLayout;
    QSpinBox *kac_dal;
    QPushButton *dal_enter;
    QWidget *alkol;
    QVBoxLayout *verticalLayout_3;
    QSpinBox *alkolfiyat;
    QPushButton *alkoltlekle;
    QWidget *hedefler;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *sigarahedef;
    QVBoxLayout *verticalLayout_5;
    QLabel *sigarayazi;
    QGroupBox *alkolhedef;
    QVBoxLayout *verticalLayout_6;
    QLabel *alkolyazi;
    QWidget *hakkinda;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(395, 305);
        MainWindow->setMinimumSize(QSize(395, 305));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        kategori = new QTabWidget(centralWidget);
        kategori->setObjectName(QStringLiteral("kategori"));
        sigara = new QWidget();
        sigara->setObjectName(QStringLiteral("sigara"));
        gridLayout = new QGridLayout(sigara);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        dal_counter = new QLCDNumber(sigara);
        dal_counter->setObjectName(QStringLiteral("dal_counter"));
        dal_counter->setFrameShape(QFrame::Box);
        dal_counter->setSmallDecimalPoint(false);
        dal_counter->setDigitCount(3);
        dal_counter->setMode(QLCDNumber::Dec);
        dal_counter->setSegmentStyle(QLCDNumber::Filled);
        dal_counter->setProperty("intValue", QVariant(0));

        gridLayout->addWidget(dal_counter, 0, 0, 1, 2);

        paket = new QGroupBox(sigara);
        paket->setObjectName(QStringLiteral("paket"));
        verticalLayout_2 = new QVBoxLayout(paket);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        yirmidal = new QPushButton(paket);
        yirmidal->setObjectName(QStringLiteral("yirmidal"));

        verticalLayout_2->addWidget(yirmidal);


        gridLayout->addWidget(paket, 1, 1, 1, 1);

        sarma = new QGroupBox(sigara);
        sarma->setObjectName(QStringLiteral("sarma"));
        verticalLayout = new QVBoxLayout(sarma);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        kac_dal = new QSpinBox(sarma);
        kac_dal->setObjectName(QStringLiteral("kac_dal"));
        kac_dal->setReadOnly(false);
        kac_dal->setButtonSymbols(QAbstractSpinBox::PlusMinus);
        kac_dal->setMinimum(1);

        verticalLayout->addWidget(kac_dal);

        dal_enter = new QPushButton(sarma);
        dal_enter->setObjectName(QStringLiteral("dal_enter"));

        verticalLayout->addWidget(dal_enter);


        gridLayout->addWidget(sarma, 1, 0, 1, 1);

        kategori->addTab(sigara, QString());
        alkol = new QWidget();
        alkol->setObjectName(QStringLiteral("alkol"));
        verticalLayout_3 = new QVBoxLayout(alkol);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        alkolfiyat = new QSpinBox(alkol);
        alkolfiyat->setObjectName(QStringLiteral("alkolfiyat"));
        alkolfiyat->setMinimumSize(QSize(353, 0));

        verticalLayout_3->addWidget(alkolfiyat);

        alkoltlekle = new QPushButton(alkol);
        alkoltlekle->setObjectName(QStringLiteral("alkoltlekle"));

        verticalLayout_3->addWidget(alkoltlekle);

        kategori->addTab(alkol, QString());
        hedefler = new QWidget();
        hedefler->setObjectName(QStringLiteral("hedefler"));
        verticalLayout_4 = new QVBoxLayout(hedefler);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        sigarahedef = new QGroupBox(hedefler);
        sigarahedef->setObjectName(QStringLiteral("sigarahedef"));
        verticalLayout_5 = new QVBoxLayout(sigarahedef);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        sigarayazi = new QLabel(sigarahedef);
        sigarayazi->setObjectName(QStringLiteral("sigarayazi"));

        verticalLayout_5->addWidget(sigarayazi);


        verticalLayout_4->addWidget(sigarahedef);

        alkolhedef = new QGroupBox(hedefler);
        alkolhedef->setObjectName(QStringLiteral("alkolhedef"));
        verticalLayout_6 = new QVBoxLayout(alkolhedef);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        alkolyazi = new QLabel(alkolhedef);
        alkolyazi->setObjectName(QStringLiteral("alkolyazi"));

        verticalLayout_6->addWidget(alkolyazi);


        verticalLayout_4->addWidget(alkolhedef);

        kategori->addTab(hedefler, QString());
        hakkinda = new QWidget();
        hakkinda->setObjectName(QStringLiteral("hakkinda"));
        kategori->addTab(hakkinda, QString());

        horizontalLayout->addWidget(kategori);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 395, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        kategori->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        paket->setTitle(QApplication::translate("MainWindow", "Paket", nullptr));
        yirmidal->setText(QApplication::translate("MainWindow", "+", nullptr));
        sarma->setTitle(QApplication::translate("MainWindow", "Sarma", nullptr));
        dal_enter->setText(QApplication::translate("MainWindow", "+", nullptr));
        kategori->setTabText(kategori->indexOf(sigara), QApplication::translate("MainWindow", "Sigara", nullptr));
        alkoltlekle->setText(QApplication::translate("MainWindow", "+", nullptr));
        kategori->setTabText(kategori->indexOf(alkol), QApplication::translate("MainWindow", "Alkol", nullptr));
        sigarahedef->setTitle(QApplication::translate("MainWindow", "Sigara", nullptr));
        sigarayazi->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        alkolhedef->setTitle(QApplication::translate("MainWindow", "Alkol", nullptr));
        alkolyazi->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        kategori->setTabText(kategori->indexOf(hedefler), QApplication::translate("MainWindow", "Hedefler", nullptr));
        kategori->setTabText(kategori->indexOf(hakkinda), QApplication::translate("MainWindow", "Hakk\304\261nda", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
