#ifndef FRP_H
#define FRP_H

#include <vector>

using namespace std;

class FRP
{
public:
	void bubble_sort(int array[], int array_size);
	void bubble_sort(vector<int> &vector, int vector_size);
	int random_dice(int dice_side);
	int stat_calc();
	void char_stat_gen(vector<int> &vector);
	void show_stats(FRP player);
	void show_stats(FRP player,ofstream &out);
	void stat_check(FRP player);

	string Name;
	string Race;
	string Class;
	string Deity;
	int Strength;
	int Dexterity;
	int Constitution;
	int Wisdom;
	int	Intelligence;
	int Charisma;
	string strcheck;
	string dexcheck;
	string concheck;
	string wischeck;
	string intcheck;
	string charcheck;
};

#endif FRP_H